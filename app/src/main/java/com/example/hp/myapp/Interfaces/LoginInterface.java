package com.example.hp.myapp.Interfaces;

import com.example.hp.myapp.Modals.LoginModel;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.PUT;
import retrofit2.http.QueryMap;

/**
 * Created by hp on 7/31/2017.
 */

public interface LoginInterface {

    @PUT ("user")
    Call<LoginModel> updateUser(@QueryMap Map<String, String> params);
}
