package com.example.hp.myapp.Utilities;

public class Constants {

    //GCM Constants
    public static final String PROJECT_NUMBER = "883551997645";
    public static final String TAG = "Shopatlocal";

    //shared preferences constants
    public static final String SHARED_PREFERENCES = "shared_preferences";
    public static final String IS_LOGGED_IN = "logged_in";
    public static final String USER_DETAIL ="user_detail";
    public static final String LOC_DATA ="loc_data";
}
