package com.example.hp.myapp.Adaptors;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.hp.myapp.R;

public class NaviAdaptor extends ArrayAdapter {
    String[] items ;

    public NaviAdaptor(@NonNull Context context, String[] items) {
        super(context, R.layout.activity_navi_adaptor, items);
        this.items=items;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_navi_adaptor,null);
        TextView item_name = view.findViewById(R.id.navi_item_name);
        item_name.setText(items[position]);
        return view;

    }
}
