package com.example.hp.myapp.Activities;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.hp.myapp.Modals.Filetobitmap;
import com.example.hp.myapp.R;
import com.example.hp.myapp.Utilities.AskPermissions;
import com.example.hp.myapp.Utilities.Constants;
import com.example.hp.myapp.Utilities.MyProgressDialog;
import com.example.hp.myapp.Utilities.NetworkConstant;
import com.example.hp.myapp.Utilities.PhotoMultipartRequest;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;

import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static com.example.hp.myapp.Utilities.AskPermissions.CAMERA_PERMISSION_CODE;
import static com.example.hp.myapp.Utilities.AskPermissions.STORAGE_PERMISSION_CODE;

public class IdUploadActivity extends AppCompatActivity implements View.OnClickListener {
    int PICKFILE_RESULT_CODE = 1;
    ImageView businesscardIV, electricbillIV, telebillIV;
    TextView anyoter;
    EditText shopreg_no, gst_no;
    Button id_save_btn, id_update_btn;
    private int PICK_IMAGE_REQUEST = 1;
    private int PICK_IMAGE_REQUEST1 = 2;
    private int PICK_IMAGE_REQUEST2 = 3;
    Bitmap uploadImageBitmap, uploadImageBitmap2, uploadImageBitmap3;
    int i;
    private String captured_image;
    HashMap<Integer, Bitmap> bitmapHashMap1 = new HashMap<>();
    HashMap<Integer, Bitmap> bitmapHashMap2 = new HashMap<>();
    HashMap<Integer, Bitmap> bitmapHashMap3 = new HashMap<>();
    SharedPreferences clientDetailId, idsharepre;
    String business, electri, telepho = null;
    int mStatusCode;
    String docFilePath, file_desti;
    private int PICK_PDF_REQUEST = 1;
    private Uri filePath;

    //storage permission code


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_id_upload);
        settooldata();
        AskPermissions.askReadExternalStoragePermission(this);

        clientDetailId = getSharedPreferences(Constants.USER_DETAIL, MODE_PRIVATE);
        idsharepre = getSharedPreferences(Constants.USER_DETAIL, MODE_PRIVATE);
        init();
        if (idsharepre.contains("id_client_id")) {
            id_save_btn.setVisibility(View.INVISIBLE);

        }

    }

    private void init() {
        businesscardIV = findViewById(R.id.bus_card_imagetv1);
        electricbillIV = findViewById(R.id.electric_imagetv1);
        telebillIV = findViewById(R.id.tele_phone_imagetv1);
        Button browser = findViewById(R.id.browser);
        shopreg_no = findViewById(R.id.shopregnumtxt);
        gst_no = findViewById(R.id.taxestxt);
        anyoter = findViewById(R.id.anyothertxt);
        id_save_btn = findViewById(R.id.id_submit_btn);
        id_update_btn = findViewById(R.id.id_update_submit_btn);
        browser.setOnClickListener(this);
        businesscardIV.setOnClickListener(this);
        electricbillIV.setOnClickListener(this);
        telebillIV.setOnClickListener(this);
        id_save_btn.setOnClickListener(this);
        id_update_btn.setOnClickListener(this);
    }

    private void settooldata() {
        ImageView imageView = findViewById(R.id.toolimage);
        TextView tooltxt = findViewById(R.id.tooltxt);
        tooltxt.setText("Upload Id");
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.browser:
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("file/*");
                startActivityForResult(intent, 132);
                break;
            case R.id.bus_card_imagetv1:
                AskPermissions.askCameraPermission(this);
                showImageChooser();
                break;
            case R.id.electric_imagetv1:
                AskPermissions.askCameraPermission(this);
                showImageChooser1();
                break;
            case R.id.tele_phone_imagetv1:
                AskPermissions.askCameraPermission(this);
                showImageChooser2();
                break;
            case R.id.id_submit_btn:
                uploadIdapi();
                break;
            case R.id.id_update_submit_btn:
                updateIdapi();
                break;
        }
    }

    private void showImageChooser() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.chooser_dialouge);
        LinearLayout CameraBtn = (LinearLayout) dialog.findViewById(R.id.ll_camera_parent);
        LinearLayout GalleryBtn = (LinearLayout) dialog.findViewById(R.id.ll_gallery_parent);
        CameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, 11);
            }
        });
        GalleryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Select File"), PICK_IMAGE_REQUEST);
            }
        });
        dialog.show();

    }

    private void showImageChooser1() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.chooser_dialouge);
        LinearLayout CameraBtn = (LinearLayout) dialog.findViewById(R.id.ll_camera_parent);
        LinearLayout GalleryBtn = (LinearLayout) dialog.findViewById(R.id.ll_gallery_parent);
        CameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, 22);

            }
        });
        GalleryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Select File"), PICK_IMAGE_REQUEST1);
            }
        });
        dialog.show();
//
//
//       /* Intent intent = new Intent();
//        intent.setType("image*//*");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST1);
//  */
    }

    //
    private void showImageChooser2() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.chooser_dialouge);
        LinearLayout CameraBtn = (LinearLayout) dialog.findViewById(R.id.ll_camera_parent);
        LinearLayout GalleryBtn = (LinearLayout) dialog.findViewById(R.id.ll_gallery_parent);
        CameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, 33);

            }
        });
        GalleryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Select File"), PICK_IMAGE_REQUEST2);
            }
        });
        dialog.show();

       /* Intent intent = new Intent();
        intent.setType("image*//*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST2);
  */
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                //Getting the Bitmap from Gallery
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                bitmap.recycle();
//                profileImageBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                uploadImageBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                bitmapHashMap1.put(1, uploadImageBitmap);

                //Setting the Bitmap to ImageView
                businesscardIV.setImageBitmap(uploadImageBitmap);
                request(bitmapHashMap1, "business");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 11 && resultCode == RESULT_OK) {
            // Uri filePath = data.getData();
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            byte[] byteArray = bytes.toByteArray();

            uploadImageBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            bitmapHashMap1.put(1, uploadImageBitmap);

            File destination = new File(Environment.getExternalStorageDirectory(),
                    System.currentTimeMillis() + ".jpg");

            FileOutputStream fo;
            try {
                destination.createNewFile();
                fo = new FileOutputStream(destination);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Log.d("tumb",destination.toString());

            businesscardIV.setImageBitmap(thumbnail);
            request(bitmapHashMap1,"");
//            try {
//
//                uploadImageBitmap = BitmapFactory.decodeFile(captured_image);
//                bitmapHashMap1.put(1, uploadImageBitmap);
//                businesscardIV.setImageBitmap(uploadImageBitmap);
//                request(bitmapHashMap1, "business");
//                Log.e("hashmap Size", bitmapHashMap1.size() + "");
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
        } else if (requestCode == PICK_IMAGE_REQUEST1 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                //Getting the Bitmap from Gallery
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                bitmap.recycle();
//                profileImageBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                uploadImageBitmap2 = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                bitmapHashMap2.put(2, uploadImageBitmap2);
                //Setting the Bitmap to ImageView
                electricbillIV.setImageBitmap(uploadImageBitmap2);
                request(bitmapHashMap2, "electric");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 22 && resultCode == RESULT_OK) {
            // Uri filePath = data.getData();
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            byte[] byteArray = bytes.toByteArray();

            uploadImageBitmap2 = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            bitmapHashMap2.put(1, uploadImageBitmap2);

            File destination = new File(Environment.getExternalStorageDirectory(),
                    System.currentTimeMillis() + ".jpg");

            FileOutputStream fo;
            try {
                destination.createNewFile();
                fo = new FileOutputStream(destination);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Log.d("tumb",destination.toString());

            electricbillIV.setImageBitmap(thumbnail);
            request(bitmapHashMap2,"");
//            try {
//                uploadImageBitmap2 = BitmapFactory.decodeFile(captured_image);
//                bitmapHashMap2.put(2, uploadImageBitmap2);
//                electricbillIV.setImageBitmap(uploadImageBitmap2);
//                request(bitmapHashMap2, "electric");
//                Log.e("hashmap Size", bitmapHashMap2.size() + "");
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
        } else if (requestCode == PICK_IMAGE_REQUEST2 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                //Getting the Bitmap from Gallery
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                bitmap.recycle();
//                profileImageBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                uploadImageBitmap3 = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                bitmapHashMap3.put(3, uploadImageBitmap3);

                //Setting the Bitmap to ImageView
                telebillIV.setImageBitmap(uploadImageBitmap3);
                request(bitmapHashMap3, "telephone");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 33 && resultCode == RESULT_OK) {
            // Uri filePath = data.getData();
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            byte[] byteArray = bytes.toByteArray();

            uploadImageBitmap3 = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            bitmapHashMap3.put(1, uploadImageBitmap3);

            File destination = new File(Environment.getExternalStorageDirectory(),
                    System.currentTimeMillis() + ".jpg");

            FileOutputStream fo;
            try {
                destination.createNewFile();
                fo = new FileOutputStream(destination);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Log.d("tumb",destination.toString());

            telebillIV.setImageBitmap(thumbnail);
            request(bitmapHashMap3,"");
//            try {
//
//                uploadImageBitmap3 = BitmapFactory.decodeFile(captured_image);
//                bitmapHashMap3.put(3, uploadImageBitmap3);
//                telebillIV.setImageBitmap(uploadImageBitmap3);
//                request(bitmapHashMap3, "telephone");
//                Log.e("hashmap Size", bitmapHashMap3.size() + "");
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
        } else if (requestCode == 132 && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri fileuri = data.getData();
            Log.d("filepath123", data.toString());
            if (fileuri.equals(null)) {
                Toast.makeText(this, "file not found", Toast.LENGTH_SHORT).show();
            } else {

                docFilePath = getFileNameByUri(this, fileuri);
//                uploadMultipart();
                fileRequest();
                Log.d("filepath", getPackageName());
            }

        }
    }

    // get file path
    private String getFileNameByUri(Context context, Uri uri) {
        String filepath = "";//default fileName
        //Uri filePathUri = uri;
        File file;
        if (uri.getScheme().toString().compareTo("content") == 0) {
            Cursor cursor = context.getContentResolver().query(uri, new String[]{android.provider.MediaStore.Images.ImageColumns.DATA, MediaStore.Images.Media.ORIENTATION}, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

            cursor.moveToFirst();

            String mImagePath = cursor.getString(column_index);
            cursor.close();
            filepath = mImagePath;
        } else if (uri.getScheme().compareTo("file") == 0) {
            try {
                file = new File(new URI(uri.toString()));
                if (file.exists())
                    filepath = file.getAbsolutePath();

            } catch (URISyntaxException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            filepath = uri.getPath();
        }
        return filepath;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void request(HashMap<Integer, Bitmap> bitmapHashMap, final String type) {

        MyProgressDialog.showDialog(this);
        MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
        File file = null;
        if (bitmapHashMap == null) {
            Toast.makeText(IdUploadActivity.this, "select image", Toast.LENGTH_SHORT).show();
        } else {

            for (Bitmap bitmap : bitmapHashMap.values()) {
                bitmap.getAllocationByteCount();
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

                Log.e("rsponse..............", bitmap.toString());
                file = Filetobitmap.bitmapToFile(IdUploadActivity.this, "file" + i + ".jpeg", bitmap, true);

                if (file != null) {
                    entityBuilder.addBinaryBody(file.getName() + i, file, ContentType.create(file.getName() + "/jpeg"), file.getName() + ".jpeg");
                    i++;
                    Log.e("rsponse.....", file.toString());

                }
            }
        }

        entityBuilder.addPart("uploadFile", new FileBody(new File(file.getPath())));
        entityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        entityBuilder.setLaxMode().setBoundary("xx").setCharset(Charset.forName("UTF-8"));
        Log.e("rsponse", entityBuilder.toString());


        final PhotoMultipartRequest request = new PhotoMultipartRequest(NetworkConstant.upload_image, entityBuilder, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MyProgressDialog.hideDialog();
                Log.e("rsponse", "onResponse " + response);
                try {
                    String status_code = response.getString("status");
                    Log.d("response_code12", status_code);
                    if (status_code.equals("200")) {
                        JSONArray jsonArray = response.getJSONArray("file");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            String file_desti = jsonObject1.getString("fd");
                            if (type.equals("business")) {
                                business = jsonObject1.getString("fd");
                            }
                            if (type.equals("electric")) {
                                electri = jsonObject1.getString("fd");
                            }
                            if (type.equals("telephone")) {
                                telepho = jsonObject1.getString("fd");

                            }
                            Log.d("file_desti", file_desti);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

//

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    MyProgressDialog.hideDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (error.getMessage() != null)
                        Toast.makeText(IdUploadActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

                }
            }
        });
        request.setRetryPolicy(new DefaultRetryPolicy(10000000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue imagequeue = Volley.newRequestQueue(this);
        imagequeue.add(request);

    }

    private void uploadIdapi() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, NetworkConstant.id_proof_save, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {

                Log.e("response", s);
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(s);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    String id = jsonObject.getString("id");
//                        String pass = jsonObject.getString("encryptedPassword");
                    Log.d("id_value", id);
                    SharedPreferences.Editor editor = idsharepre.edit();
                    editor.putString("id_client_id", id);
                    editor.apply();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("clientDetailId", clientDetailId.getString("update_id", ""));
                params.put("businessCard", URLEncoder.encode(business));
                params.put("electricityBill", URLEncoder.encode(electri));
                params.put("telephoneBill", URLEncoder.encode(telepho));
                params.put("shopRegistrationNo", URLEncoder.encode(shopreg_no.getText().toString()));
                params.put("taxes", URLEncoder.encode(gst_no.getText().toString()));
                params.put("other", file_desti);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void updateIdapi() {
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, NetworkConstant.id_proof_update, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.e("response", s);
                Toast.makeText(IdUploadActivity.this, "Data Updates Successfully", Toast.LENGTH_SHORT).show();
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }) {
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                mStatusCode = response.statusCode;
                return super.parseNetworkResponse(response);
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", idsharepre.getString("id_client_id", ""));
                params.put("businessCard", URLEncoder.encode(business));
                params.put("electricityBill", URLEncoder.encode(electri));
                params.put("telephoneBill", URLEncoder.encode(telepho));
                params.put("shopRegistrationNo", URLEncoder.encode(shopreg_no.getText().toString()));
                params.put("taxes", URLEncoder.encode(gst_no.getText().toString()));
                params.put("other", file_desti);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    //Requesting permission
//    private void requestStoragePermission() {
//        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
//            return;
//
//        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
//            //If the user has denied the permission previously your code will come to this block
//            //Here you can explain why you need this permission
//            //Explain here why you need this permission
//        }
//        //And finally ask for the permission
//        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
//    }


    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == STORAGE_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
//                Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
        else {

        }
         if (requestCode==CAMERA_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
//                Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void fileRequest() {

        MyProgressDialog.showDialog(this);
        MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
        File file = null;

        if (file != null) {
            entityBuilder.addBinaryBody(file.getName() + i, file, ContentType.create(file.getName() + "/jpeg"), file.getName() + ".jpeg");
            i++;
            Log.e("rsponse...........", file.toString());

        }
        entityBuilder.addPart("uploadFile", new FileBody(new File(docFilePath)));
        entityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

        entityBuilder.setLaxMode().setBoundary("xx").setCharset(Charset.forName("UTF-8"));
        Log.e("rsponse", entityBuilder.toString());


        final PhotoMultipartRequest request = new PhotoMultipartRequest(NetworkConstant.upload_image, entityBuilder, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MyProgressDialog.hideDialog();
                Log.e("rsponse", "onResponse " + response);
                try {
                    String status_code = response.getString("status");
                    Log.d("response_code12", status_code);
                    if (status_code.equals("200")) {
                        JSONArray jsonArray = response.getJSONArray("file");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            file_desti = jsonObject1.getString("fd");
                            String filename = jsonObject1.getString("filename");
                            anyoter.setText(filename);
                            if (file_desti.equals("")) {

                            } else {
                                Log.d("file_desti", file_desti);
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    MyProgressDialog.hideDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (error.getMessage() != null)
                        Toast.makeText(IdUploadActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

                }
            }
        });
        request.setRetryPolicy(new DefaultRetryPolicy(10000000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue imagequeue = Volley.newRequestQueue(this);
        imagequeue.add(request);
    }
}

