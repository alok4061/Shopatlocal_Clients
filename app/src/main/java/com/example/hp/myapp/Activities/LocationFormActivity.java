package com.example.hp.myapp.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.hp.myapp.R;
import com.example.hp.myapp.Utilities.Constants;
import com.example.hp.myapp.Utilities.NetworkConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;



public class LocationFormActivity extends AppCompatActivity implements View.OnClickListener,LocationListener {
    SharedPreferences sharedPreferences,loca_update_database;
    String clientDetailid,latStr,lngstr;
    EditText arestxt, addresstxt, landmarktxt, pincodetxt;
    Spinner countryspin, statespin, cityspin;
    Button savebtn,updatebtn;
    int mStatusCode;
    double latitude,longitude;
    LocationManager locationManager;
    private RequestQueue savequeue,updatequeue;
    Location  location;
    ArrayList countrylist = new ArrayList();
    ArrayList satatlist = new ArrayList();
    ArrayList citylist = new ArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_form);
        requestStoragePermission();
        savequeue = Volley.newRequestQueue(this);
        updatequeue= Volley.newRequestQueue(this);
        getlatlng();
        sharedPreferences = getSharedPreferences(Constants.USER_DETAIL, MODE_PRIVATE);
        clientDetailid = sharedPreferences.getString("update_id","");
        loca_update_database =getSharedPreferences(Constants.USER_DETAIL,MODE_PRIVATE);
        settooldata();
        init();
        if (loca_update_database.contains("Location_client_id")){
            Log.d("hcdhcbh",loca_update_database.getString("Location_client_id",""));
            savebtn.setVisibility(View.INVISIBLE);
        }
        additems();
    }
    private void additems(){

        //city
        countrylist.add("Select Country Name");
        countrylist.add("India");
        countrylist.add("U.S.");
        countrylist.add("U.K.");
        countrylist.add("Canada");
        //satate
        satatlist.add("Select state name");
        satatlist.add("Tamil Nadu");
        satatlist.add("gujarat");
        satatlist.add("U.p");
        satatlist.add("u.k");

        //Tenure
        citylist.add("Select Tenure");
        citylist.add("Mumbai");
        citylist.add("Delhi");
        citylist.add("Kolkata");

        ArrayAdapter<String> owneradaptor = new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,countrylist);
        ArrayAdapter<String> tymarketadaptor = new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,satatlist);
        ArrayAdapter<String> tyofvenueadaptor = new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,citylist);
        countryspin.setAdapter(owneradaptor);
        statespin.setAdapter(tymarketadaptor);
        cityspin.setAdapter(tyofvenueadaptor);
    }
    private void init() {
        arestxt = findViewById(R.id.areatxt);
        addresstxt = findViewById(R.id.addresstxt);
        landmarktxt = findViewById(R.id.landmarktxt);
        pincodetxt = findViewById(R.id.pincodetxt);
        countryspin = findViewById(R.id.countryspin);
        statespin = findViewById(R.id.statespin);
        cityspin = findViewById(R.id.cityspin);
        savebtn = findViewById(R.id.save_loc_btn);
        updatebtn =findViewById(R.id.update_loc_btn);
        savebtn.setOnClickListener(this);
    }
    private void settooldata() {
        ImageView imageView = findViewById(R.id.toolimage);
        TextView tooltxt = findViewById(R.id.tooltxt);
        tooltxt.setText("Location Infomation");
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.save_loc_btn:
                getLocation_api();
                break;
            case R.id.update_loc_btn:
                updateLocation_api();
                break;
        }

    }

    private void getLocation_api() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST,NetworkConstant.locationapi, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.d("res123", s);
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(s);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    if (mStatusCode == 200) {
                        String id = jsonObject.getString("id");
//                        String pass = jsonObject.getString("encryptedPassword");
                        SharedPreferences.Editor editor = loca_update_database.edit();
                        editor.putString("Location_client_id", id);
                        editor.apply();
                        Toast.makeText(LocationFormActivity.this," Location Details Successfully Saved",Toast.LENGTH_SHORT).show();
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("res123.......", volleyError.toString());
            }
        }) {
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                mStatusCode = response.statusCode;
                Log.d("res123.......", mStatusCode + "");
                return super.parseNetworkResponse(response);
            }
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("clientDetailId", clientDetailid);
                params.put("city", URLEncoder.encode(cityspin.getSelectedItem().toString()));
                params.put("landmark", URLEncoder.encode(landmarktxt.getText().toString()));
                params.put("area", URLEncoder.encode(arestxt.getText().toString()));
                params.put("pincode", URLEncoder.encode(pincodetxt.getText().toString()));
//                params.put("country", URLEncoder.encode(countryspin.getSelectedItem().toString()));
//                params.put("state", URLEncoder.encode(statespin.getSelectedItem().toString()));
                params.put("longitude", 28.6618976+"");
                params.put("latitude",77.22739580000007+"");

                Log.d("params", params.toString());
                return params;
            }
        };
        savequeue.add(stringRequest);
    }
    private void updateLocation_api() {
        StringRequest stringRequest = new StringRequest(Request.Method.PUT,NetworkConstant.locationapi, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.d("res123", s);
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(s);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                    if (mStatusCode == 200) {
//                        String id = jsonObject.getString("id");
//                        String pass = jsonObject.getString("encryptedPassword");

//                        SharedPreferences.Editor editor = sharedPreferences.edit();
//                        editor.putString("Location_client_id", id);
//                        editor.apply();
                        Toast.makeText(LocationFormActivity.this,"Location Details Successfully Updated",Toast.LENGTH_SHORT).show();
                        finish();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(LocationFormActivity.this,"Please Try Again\n"+volleyError,Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                mStatusCode = response.statusCode;
                return super.parseNetworkResponse(response);
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("clientDetailId", loca_update_database.getString("Location_client_id",""));
//                params.put("city", URLEncoder.encode(cityspin.getSelectedItem().toString()));
                params.put("landmark", URLEncoder.encode(landmarktxt.getText().toString()));
                params.put("area", URLEncoder.encode(arestxt.getText().toString()));
                params.put("pincode", URLEncoder.encode(pincodetxt.getText().toString()));
                params.put("country", URLEncoder.encode(cityspin.getSelectedItem().toString()));
                params.put("state", URLEncoder.encode(statespin.getSelectedItem().toString()));
                params.put("longitude", 28.6618976+"");
                params.put("latitude",77.22739580000007+"");
                Log.d("params", params.toString());
                return params;
            }
        };
        updatequeue.add(stringRequest);
    }

    private void getlatlng() {
        locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        if (location != null) {
            latitude=location.getLatitude();
            longitude=location.getLongitude();
            latStr = String.valueOf(latitude);
            lngstr = String.valueOf(longitude);
            Log.d("old","lat :  "+latitude);
            Log.d("old","long :  "+longitude);
            this.onLocationChanged(location);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        latitude=location.getLatitude();
        longitude=location.getLongitude();
        Log.d("old","lat :  "+latitude);
        Log.d("old","long :  "+longitude);

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
    //Requesting permission
    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            return;

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 12);
    }


    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == 12) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
//                Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
    }
}

