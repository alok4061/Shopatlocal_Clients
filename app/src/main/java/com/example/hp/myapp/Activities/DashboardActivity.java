package com.example.hp.myapp.Activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.hp.myapp.Adaptors.NaviAdaptor;
import com.example.hp.myapp.R;
import com.example.hp.myapp.Utilities.Constants;
import com.example.hp.myapp.Utilities.MyProgressDialog;
import com.example.hp.myapp.Utilities.NetworkConstant;
import com.google.android.gms.maps.LocationSource;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class DashboardActivity extends AppCompatActivity
        implements AdapterView.OnItemClickListener,View.OnClickListener,LocationListener {
    String[] items ={"Personal Info","Id proof","Location","photos","Offers","Notification","Setting","Upgrade to Pro","Logout"};
    ListView listView;
    DrawerLayout drawer;
    SharedPreferences sharedPreferences,clientdetailprefrence,locshareprefrence,proofshareprefrence,infoproofsharedprefrence;
    Button locbtn,id_proof,ch_photos,offers,personal,additional;
    double latitude=0.0,longitude=0.0;
    LocationManager locationManager;
    Location location;
    String latStr,lngstr;
    SharedPreferences locprefrence;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard2);
        getlatlng();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        locshareprefrence = getSharedPreferences(Constants.LOC_DATA,MODE_PRIVATE);
        sharedPreferences = getSharedPreferences(Constants.USER_DETAIL,MODE_PRIVATE);
        locshareprefrence = getSharedPreferences(Constants.USER_DETAIL,MODE_PRIVATE);
        proofshareprefrence = getSharedPreferences(Constants.USER_DETAIL,MODE_PRIVATE);
        infoproofsharedprefrence = getSharedPreferences(Constants.USER_DETAIL,MODE_PRIVATE);
        clientdetailprefrence = getSharedPreferences(Constants.USER_DETAIL,MODE_PRIVATE);
//        checkIds();
        setSupportActionBar(toolbar);
        listView = findViewById(R.id.navi_list);
        NaviAdaptor naviAdaptor = new NaviAdaptor(this,items);
        listView.setAdapter(naviAdaptor);
        addlistner();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
    }
    @Override
    protected void onStart() {
        super.onStart();

    }

    private void addlistner(){
        listView.setOnItemClickListener(this);
        locbtn = findViewById(R.id.locationform_btn);
        id_proof =findViewById(R.id.id_btn);
        ch_photos = findViewById(R.id.photos_btn);
        personal = findViewById(R.id.personal_btn);
        offers = findViewById(R.id.offers_btn);
        additional = findViewById(R.id.addition_btn);
        personal.setOnClickListener(this);
        locbtn.setOnClickListener(this);
        id_proof.setOnClickListener(this);
        ch_photos.setOnClickListener(this);
        offers.setOnClickListener(this);
        additional.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);

        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        switch (i){
            case 0:
                Intent profle = new Intent(this,PersonalInfoActivity.class);
                startActivity(profle);
                break;
            case 1:
                Intent basic = new Intent(this,IdUploadActivity.class);
                startActivity(basic);
                break;
            case 2:
                Intent location = new Intent(this,LocationFormActivity.class);
                startActivity(location);
                break;
            case 3:
                Intent proof = new Intent(this,PhotosChooserActivity.class);
                startActivity(proof);
                break;
            case 4:
                Intent photoes = new Intent(this,OffersActivity.class);
                startActivity(photoes);
                break;
            case 5:
                Intent Notifie = new Intent(this,NotificationsActivity.class);
                startActivity(Notifie);
                break;
            case 6:
                Intent setting = new Intent(this,SettingActivity.class);
                startActivity(setting);
                break;
            case 7:
                Intent pero = new Intent(this,UpgradetoPRO.class);
                startActivity(pero);
                break;
                case 8:
                Intent logout = new Intent(this,MainActivity.class);
                    SharedPreferences preferences =getSharedPreferences(Constants.USER_DETAIL,MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.clear();
                    editor.commit();
                    finish();
                startActivity(logout);
                break;

        }
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.locationform_btn:
                if (clientdetailprefrence.contains("update_id")){
                    Intent location = new Intent(this,LocationFormActivity.class);
                    startActivity(location);
                }
                else {
                    Toast.makeText(this,"Please first fill the Personal Informations",Toast.LENGTH_LONG).show();
                }

                break;
            case R.id.id_btn:
                if (clientdetailprefrence.contains("update_id")){
                    Intent id = new Intent(this,IdUploadActivity.class);
                    startActivity(id);
                }
                else {
                    Toast.makeText(this,"Please first fill the Personal Informations",Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.photos_btn:
                if (clientdetailprefrence.contains("update_id")){
                    Intent photos = new Intent(this,PhotosChooserActivity.class);
                    startActivity(photos);
                }
                else {
                    Toast.makeText(this,"Please first fill the Personal Informations",Toast.LENGTH_LONG).show();
                }

                break;
            case R.id.personal_btn:
                Intent persomal_info = new Intent(this,PersonalInfoActivity.class);
                    startActivity(persomal_info);
                break;
            case R.id.offers_btn:
                if (clientdetailprefrence.contains("update_id")){
                    Intent offers = new Intent(this,OffersActivity.class);
                    startActivity(offers);
                }
                else {
                    Toast.makeText(this,"Please first fill the Personal Informations",Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.addition_btn:
                if (clientdetailprefrence.contains("update_id")){
                    Intent offers = new Intent(this,AdditionalInfoActivity.class);
                    startActivity(offers);
                }
                else {
                    Toast.makeText(this,"Please first fill the Personal Informations",Toast.LENGTH_LONG).show();
                }
        }
    }

//    private void checkIds(){
//        MyProgressDialog.showDialog(this);
//        JsonObjectRequest jsonArrayRequest = new JsonObjectRequest(Request.Method.GET, NetworkConstant.check_id + "?id="+sharedPreferences.getString("User_id",""), null, new Response.Listener<JSONObject>() {
//
//            @Override
//            public void onResponse(JSONObject jsonObject) {
//                MyProgressDialog.hideDialog();
//
//                try {
//                    String clientid =jsonObject.getString("clientId");
//                    String clientdetailid =jsonObject.getString("clientDetailId");
//                    String locid =jsonObject.getString("locationId");
//                    String infoid =jsonObject.getString("infoId");
//                    String proofid =jsonObject.getString("proofId");
//                    Log.d("idvalues",clientdetailid+locid+infoid+proofid);
//                    SharedPreferences.Editor clientdetail = clientdetailprefrence.edit();
//                    SharedPreferences.Editor loc_edit = locshareprefrence.edit();
//                    SharedPreferences.Editor proof_edit = proofshareprefrence.edit();
//                    SharedPreferences.Editor info_edit =infoproofsharedprefrence.edit();
//                    clientdetail.putString("update_id",clientdetailid);
//                    loc_edit.putString("Location_client_id",locid);
//                    proof_edit.putString("id_client_id",proofid);
//                    info_edit.putString("info_id",infoid);
//                    clientdetail.commit();
//                    loc_edit.commit();
//                    proof_edit.commit();
//                    info_edit.commit();
//
//                    Log.d("id's",clientdetailprefrence.getString("update_id","")  +locshareprefrence.getString("Location_client_id","")
//                            +proofshareprefrence.getString("id_client_id","")+infoproofsharedprefrence.getString("info_id",""));
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError volleyError) {
//                MyProgressDialog.hideDialog();
//            }
//        });
//        RequestQueue requestQueue = Volley.newRequestQueue(this);
//        requestQueue.add(jsonArrayRequest);
//    }

    private void getlatlng() {
        locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        try {
            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (location != null) {
                latitude=location.getLatitude();
                longitude=location.getLongitude();
                latStr = String.valueOf(latitude);
                lngstr = String.valueOf(longitude);
                Log.d("old","lat :  "+latitude);
                Log.d("old1","long :  "+longitude);
                editor = locprefrence.edit();
                editor.putString("lat",latitude+"");
                editor.putString("lng",longitude+"");
                editor.commit();
                this.onLocationChanged(location);
            }
        }
        catch (Exception e){

        }

    }

    @Override
    public void onLocationChanged(Location location) {
        latitude=location.getLatitude();
        longitude=location.getLongitude();
        Log.d("old","lat :  "+latitude);
        Log.d("old1","long :  "+longitude);
        editor = locprefrence.edit();
        editor.putString("lat",latitude+"");
        editor.putString("lng",longitude+"");
        editor.commit();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
