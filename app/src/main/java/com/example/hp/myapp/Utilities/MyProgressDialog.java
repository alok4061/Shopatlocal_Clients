package com.example.hp.myapp.Utilities;

/**
 * Created by ravi on 14/5/16.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;


import android.util.Log;

public class MyProgressDialog {

    private static ProgressDialog pDialog = null;

    public static void showDialog(Context context) {
        if (pDialog == null) {
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Please Wait");
            pDialog.setCancelable(false);
            if (!pDialog.isShowing()) {
                pDialog.show();
            }
        }

    }

    public static void showDialog(Context context, String msg) {
        if (pDialog == null) {
            pDialog = new ProgressDialog(context);
            pDialog.setProgressDrawable(new ColorDrawable(Color.BLUE));
            pDialog.setMessage(msg);
            pDialog.setCancelable(false);
            if (!pDialog.isShowing()) {
                pDialog.show();
            }
        }
    }

    public static void hideDialog() {
        if (pDialog != null && pDialog.isShowing()) {
            try {
                pDialog.dismiss();
            } catch (Exception e) {
                Log.e("progress dialog error",e.toString());
            }
            pDialog = null;
        }
    }

}
