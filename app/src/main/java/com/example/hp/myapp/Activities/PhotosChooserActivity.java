package com.example.hp.myapp.Activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.hp.myapp.Modals.Filetobitmap;
import com.example.hp.myapp.R;
import com.example.hp.myapp.Utilities.AskPermissions;
import com.example.hp.myapp.Utilities.Constants;
import com.example.hp.myapp.Utilities.MyProgressDialog;
import com.example.hp.myapp.Utilities.NetworkConstant;
import com.example.hp.myapp.Utilities.PhotoMultipartRequest;

import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.example.hp.myapp.Utilities.AskPermissions.CAMERA_PERMISSION_CODE;
import static com.example.hp.myapp.Utilities.AskPermissions.STORAGE_PERMISSION_CODE;

public class PhotosChooserActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView imageIV1, imageIV2, imageIV3, imageIV4, imageIV5, imageIV6, imageIV7, imageIV8;
    private int PICK_IMAGE_REQUEST = 1;
    private int PICK_IMAGE_REQUEST1 = 2;
    private int PICK_IMAGE_REQUEST2 = 3;
    private int PICK_IMAGE_REQUEST3 = 4;
    private int PICK_IMAGE_REQUEST4 = 5;
    private int PICK_IMAGE_REQUEST5 = 6;
    private int PICK_IMAGE_REQUEST6 = 7;
    private int PICK_IMAGE_REQUEST7 = 8;
    int i;
    private String captured_image;
    Bitmap uploadImageBitmap, uploadImageBitmap2, uploadImageBitmap3, uploadImageBitmap4,uploadImageBitmap5, uploadImageBitmap6, uploadImageBitmap7, uploadImageBitmap8;
    RequestQueue imagequeue;
    Button uploadbtn;
    Bitmap mybitmap;
    Uri filePath;
    ArrayList picfds = new ArrayList();
    SharedPreferences sharedPreferences;
    HashMap<Integer, Bitmap> bitmapHashMap1 = new HashMap<>();
    HashMap<Integer, Bitmap> bitmapHashMap2 = new HashMap<>();
    HashMap<Integer, Bitmap> bitmapHashMap3 = new HashMap<>();
    HashMap<Integer, Bitmap> bitmapHashMap4 = new HashMap<>();
    HashMap<Integer, Bitmap> bitmapHashMap5 = new HashMap<>();
    HashMap<Integer, Bitmap> bitmapHashMap6 = new HashMap<>();
    HashMap<Integer, Bitmap> bitmapHashMap7 = new HashMap<>();
    HashMap<Integer, Bitmap> bitmapHashMap8 = new HashMap<>();
    ContentValues values;
    Uri imageUri;
    String imageurl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photos_chooser);
        imagequeue = Volley.newRequestQueue(this);
        AskPermissions.askReadExternalStoragePermission(this);
        sharedPreferences = getSharedPreferences(Constants.USER_DETAIL, MODE_PRIVATE);
        settooldata();
        init();
    }

    private void init() {
        imageIV1 = findViewById(R.id.imagetv1);
        imageIV2 = findViewById(R.id.imagetv2);
        imageIV3 = findViewById(R.id.imagetv3);
        imageIV4 = findViewById(R.id.imagetv4);
        imageIV5 = findViewById(R.id.imagetv5);
        imageIV6 = findViewById(R.id.imagetv6);
        imageIV7 = findViewById(R.id.imagetv7);
        imageIV8 = findViewById(R.id.imagetv8);
        uploadbtn = findViewById(R.id.photo_submit_btn);
        uploadImageBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.user_profile);
        uploadImageBitmap2 = BitmapFactory.decodeResource(getResources(), R.drawable.user_profile);
        uploadImageBitmap3 = BitmapFactory.decodeResource(getResources(), R.drawable.user_profile);
        uploadImageBitmap4 = BitmapFactory.decodeResource(getResources(), R.drawable.user_profile);
        imageIV1.setOnClickListener(this);
        imageIV2.setOnClickListener(this);
        imageIV3.setOnClickListener(this);
        imageIV4.setOnClickListener(this);
        imageIV5.setOnClickListener(this);
        imageIV6.setOnClickListener(this);
        imageIV7.setOnClickListener(this);
        imageIV8.setOnClickListener(this);
        uploadbtn.setOnClickListener(this);
        uploadbtn = findViewById(R.id.photo_submit_btn);
    }

    private void settooldata() {
        ImageView imageView = findViewById(R.id.toolimage);
        TextView tooltxt = findViewById(R.id.tooltxt);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imagetv1:
//               showFileChooser();
                AskPermissions.askCameraPermission(this);
                showImageChooser();
                break;
            case R.id.imagetv2:
                showImageChooser1();
                AskPermissions.askCameraPermission(this);
                break;
            case R.id.imagetv3:
                showImageChooser2();
                AskPermissions.askCameraPermission(this);
                break;
            case R.id.imagetv4:
                showImageChooser3();
                AskPermissions.askCameraPermission(this);
                break;
            case R.id.imagetv5:
                showImageChooser4();
                AskPermissions.askCameraPermission(this);
                break;
            case R.id.imagetv6:
                showImageChooser5();
                AskPermissions.askCameraPermission(this);
                break;
            case R.id.imagetv7:
                showImageChooser6();
                AskPermissions.askCameraPermission(this);
                break;
            case R.id.imagetv8:
                showImageChooser7();
                AskPermissions.askCameraPermission(this);
                break;
            case R.id.photo_submit_btn:
                uploadImageArray();



//                sendimageonserver();
//                uploadImage();
                break;

        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == STORAGE_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
//                Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
        else {

        }
        if (requestCode==CAMERA_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
//                Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
    }
//    public String getStringImage(Bitmap bmp){
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        bmp.compress(Bitmap.CompressFormat.JPEG, 40, baos);
//        byte[] imageBytes = baos.toByteArray();
//        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
//        return encodedImage;
//    }
//    private void uploadImage(){
//        //Showing the progress dialog
//        final ProgressDialog loading = ProgressDialog.show(this,"Uploading...","Please wait...",false,false);
//        StringRequest stringRequest = new StringRequest(Request.Method.POST,NetworkConstant.upload_image,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String s) {
//                        //Disimissing the progress dialog
//                        Log.e("response",s);
//                        loading.dismiss();
//                        //Showing toast message of the response
//                        Toast.makeText(PhotosChooserActivity.this, s , Toast.LENGTH_LONG).show();
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError volleyError) {
//                        //Dismissing the progress dialog
//                        loading.dismiss();
//
//                        //Showing toast
////                        Toast.makeText(PhotosChooserActivity.this, volleyError.getMessage().toString(), Toast.LENGTH_LONG).show();
//                    }
//                }){
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                //Converting Bitmap to String
//                String image = getStringImage(mybitmap);
//                Log.e("response",mybitmap.toString());
//
//
//                //Creating parameters
//                Map<String,String> params = new Hashtable<String, String>();
//
//                //Adding parameters
//                params.put("uploadFile", image);
//                Log.e("response",filePath.toString());
//
//
//                //returning parameters
//                return params;
//            }
//        };
//
//        //Creating a Request Queue
//
//
//        //Adding request to the queue
//        imagequeue.add(stringRequest);
//    }
//    private void showFileChooser() {
//        Intent intent = new Intent();
//        intent.setType("image/*");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
//    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
//            filePath = data.getData();
//            try {
//                //Getting the Bitmap from Gallery
//                mybitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
//                Log.d("mybitmap",mybitmap.toString());
//                //Setting the Bitmap to ImageView
//                imageIV1.setImageBitmap(mybitmap);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//    }
public String getRealPathFromURI1(Uri contentUri) {
    String[] proj = { MediaStore.Images.Media.DATA };
    Cursor cursor = managedQuery(contentUri, proj, null, null, null);
    int column_index = cursor
            .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
    cursor.moveToFirst();
    return cursor.getString(column_index);
}



    private void showImageChooser() {
        final Dialog dialog = new Dialog(PhotosChooserActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.chooser_dialouge);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        LinearLayout CameraBtn = (LinearLayout) dialog.findViewById(R.id.ll_camera_parent);
        LinearLayout GalleryBtn = (LinearLayout) dialog.findViewById(R.id.ll_gallery_parent);
        CameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
//                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                startActivityForResult(cameraIntent, 11);
                values = new ContentValues();
                values.put(MediaStore.Images.Media.TITLE, "New Picture");
                values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
                imageUri = getContentResolver().insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                startActivityForResult(intent, 11);
            }
        });
        GalleryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Select File"), PICK_IMAGE_REQUEST);
            }
        });
        dialog.show();

    }

    private void showImageChooser1() {
        final Dialog dialog = new Dialog(PhotosChooserActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.chooser_dialouge);
        LinearLayout CameraBtn = (LinearLayout) dialog.findViewById(R.id.ll_camera_parent);
        LinearLayout GalleryBtn = (LinearLayout) dialog.findViewById(R.id.ll_gallery_parent);
        CameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, 22);

            }
        });
        GalleryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Select File"), PICK_IMAGE_REQUEST1);
            }
        });
        dialog.show();
//
//
//       /* Intent intent = new Intent();
//        intent.setType("image*//*");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST1);
//  */
    }

//
    private void showImageChooser2() {
        final Dialog dialog = new Dialog(PhotosChooserActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.chooser_dialouge);
        LinearLayout CameraBtn = (LinearLayout) dialog.findViewById(R.id.ll_camera_parent);
        LinearLayout GalleryBtn = (LinearLayout) dialog.findViewById(R.id.ll_gallery_parent);
        CameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, 33);

            }
        });
        GalleryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Select File"), PICK_IMAGE_REQUEST2);
            }
        });
        dialog.show();

       /* Intent intent = new Intent();
        intent.setType("image*//*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST2);
  */
    }

    private void showImageChooser3() {

        final Dialog dialog = new Dialog(PhotosChooserActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.chooser_dialouge);
        LinearLayout CameraBtn = (LinearLayout) dialog.findViewById(R.id.ll_camera_parent);
        LinearLayout GalleryBtn = (LinearLayout) dialog.findViewById(R.id.ll_gallery_parent);
        CameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, 44);

            }
        });
        GalleryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Select File"), PICK_IMAGE_REQUEST3);
            }
        });
        dialog.show();
     /*   Intent intent = new Intent();
        intent.setType("image*//*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST3);
  */
    }
    private void showImageChooser4() {
        final Dialog dialog = new Dialog(PhotosChooserActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.chooser_dialouge);
        LinearLayout CameraBtn = (LinearLayout) dialog.findViewById(R.id.ll_camera_parent);
        LinearLayout GalleryBtn = (LinearLayout) dialog.findViewById(R.id.ll_gallery_parent);
        CameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, 55);

            }
        });
        GalleryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Select File"), PICK_IMAGE_REQUEST4);
            }
        });
        dialog.show();
//
//
//       /* Intent intent = new Intent();
//        intent.setType("image*//*");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST1);
//  */
    }
    private void showImageChooser5() {
        final Dialog dialog = new Dialog(PhotosChooserActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.chooser_dialouge);
        LinearLayout CameraBtn = (LinearLayout) dialog.findViewById(R.id.ll_camera_parent);
        LinearLayout GalleryBtn = (LinearLayout) dialog.findViewById(R.id.ll_gallery_parent);
        CameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, 66);

            }
        });
        GalleryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Select File"), PICK_IMAGE_REQUEST5);
            }
        });
        dialog.show();
//
//
//       /* Intent intent = new Intent();
//        intent.setType("image*//*");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST1);
//  */
    }
    private void showImageChooser6() {
        final Dialog dialog = new Dialog(PhotosChooserActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.chooser_dialouge);
        LinearLayout CameraBtn = (LinearLayout) dialog.findViewById(R.id.ll_camera_parent);
        LinearLayout GalleryBtn = (LinearLayout) dialog.findViewById(R.id.ll_gallery_parent);
        CameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, 77);

            }
        });
        GalleryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Select File"), PICK_IMAGE_REQUEST6);
            }
        });
        dialog.show();
//
//
//       /* Intent intent = new Intent();
//        intent.setType("image*//*");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST1);
//  */
    }
    private void showImageChooser7() {
        final Dialog dialog = new Dialog(PhotosChooserActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.chooser_dialouge);
        LinearLayout CameraBtn = (LinearLayout) dialog.findViewById(R.id.ll_camera_parent);
        LinearLayout GalleryBtn = (LinearLayout) dialog.findViewById(R.id.ll_gallery_parent);
        CameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, 88);

            }
        });
        GalleryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Select File"), PICK_IMAGE_REQUEST7);
            }
        });
        dialog.show();
//
//
//       /* Intent intent = new Intent();
//        intent.setType("image*//*");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST1);
//  */
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {

                //Getting the Bitmap from Gallery
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                bitmap.recycle();
//                profileImageBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                uploadImageBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                bitmapHashMap1.put(1, uploadImageBitmap);

                //Setting the Bitmap to ImageView
                imageIV1.setImageBitmap(uploadImageBitmap);
                request(bitmapHashMap1);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (requestCode == 11 && resultCode == RESULT_OK) {

            Bitmap thumbnail;
            try {
//                ShrinkBitmap(data.toString(),300,300);
                thumbnail = MediaStore.Images.Media.getBitmap(
                        getContentResolver(), imageUri);
                imageurl = getRealPathFromURI1(imageUri);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 20, bytes);
                byte[] byteArray = bytes.toByteArray();
                uploadImageBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                bitmapHashMap1.put(1, uploadImageBitmap);

                request(bitmapHashMap1);
                imageIV1.setImageBitmap(thumbnail);
            } catch (Exception e) {
                e.printStackTrace();
            }


//            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
//            Bitmap resizedBitmap = Bitmap.createScaledBitmap(
//                    thumbnail, 1024,768, false);
//            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//            resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 40, bytes);
//            byte[] byteArray = bytes.toByteArray();
//
//            uploadImageBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
//            bitmapHashMap1.put(1, uploadImageBitmap);
//            imageIV1.setImageBitmap(resizedBitmap);
//            request(bitmapHashMap1);

        }

       else if (requestCode == PICK_IMAGE_REQUEST1 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                //Getting the Bitmap from Gallery
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                bitmap.recycle();
//                profileImageBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                uploadImageBitmap2 = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                bitmapHashMap2.put(2, uploadImageBitmap2);

                //Setting the Bitmap to ImageView
                imageIV2.setImageBitmap(uploadImageBitmap2);
                request(bitmapHashMap2);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 22 && resultCode == RESULT_OK) {
            // Uri filePath = data.getData();
            Bitmap thumbnail;
            try {
//                ShrinkBitmap(data.toString(),300,300);
                thumbnail = MediaStore.Images.Media.getBitmap(
                        getContentResolver(), imageUri);
                imageurl = getRealPathFromURI1(imageUri);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 20, bytes);
                byte[] byteArray = bytes.toByteArray();
                uploadImageBitmap2 = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                bitmapHashMap2.put(2, uploadImageBitmap2);

                request(bitmapHashMap2);
                imageIV2.setImageBitmap(thumbnail);
            } catch (Exception e) {
                e.printStackTrace();
            }


        } else if (requestCode == PICK_IMAGE_REQUEST2 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                //Getting the Bitmap from Gallery
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                bitmap.recycle();
//                profileImageBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                uploadImageBitmap3 = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                bitmapHashMap3.put(3, uploadImageBitmap3);

                //Setting the Bitmap to ImageView
                imageIV3.setImageBitmap(uploadImageBitmap3);
                request(bitmapHashMap3);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 33 && resultCode == RESULT_OK) {
            // Uri filePath = data.getData();
            Bitmap thumbnail;
            try {
//                ShrinkBitmap(data.toString(),300,300);
                thumbnail = MediaStore.Images.Media.getBitmap(
                        getContentResolver(), imageUri);
                imageurl = getRealPathFromURI1(imageUri);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 20, bytes);
                byte[] byteArray = bytes.toByteArray();
                uploadImageBitmap3 = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                bitmapHashMap3.put(3, uploadImageBitmap3);

                request(bitmapHashMap3);
                imageIV3.setImageBitmap(thumbnail);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (requestCode == PICK_IMAGE_REQUEST3 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                //Getting the Bitmap from Gallery
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                bitmap.recycle();
//                profileImageBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                uploadImageBitmap4 = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                bitmapHashMap4.put(4, uploadImageBitmap4);

                //Setting the Bitmap to ImageView
                imageIV4.setImageBitmap(uploadImageBitmap4);
                request(bitmapHashMap4);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 44 && resultCode == RESULT_OK) {
            // Uri filePath = data.getData();
            Bitmap thumbnail;
            try {
//                ShrinkBitmap(data.toString(),300,300);
                thumbnail = MediaStore.Images.Media.getBitmap(
                        getContentResolver(), imageUri);
                imageurl = getRealPathFromURI1(imageUri);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 20, bytes);
                byte[] byteArray = bytes.toByteArray();
                uploadImageBitmap4 = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                bitmapHashMap4.put(4, uploadImageBitmap4);

                request(bitmapHashMap4);
                imageIV4.setImageBitmap(thumbnail);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else if (requestCode == PICK_IMAGE_REQUEST4 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                //Getting the Bitmap from Gallery
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                bitmap.recycle();
//                profileImageBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                uploadImageBitmap5 = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                bitmapHashMap5.put(5, uploadImageBitmap5);

                //Setting the Bitmap to ImageView
                imageIV5.setImageBitmap(uploadImageBitmap5);
                request(bitmapHashMap5);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 55 && resultCode == RESULT_OK) {
            // Uri filePath = data.getData();
            Bitmap thumbnail;
            try {
//                ShrinkBitmap(data.toString(),300,300);
                thumbnail = MediaStore.Images.Media.getBitmap(
                        getContentResolver(), imageUri);
                imageurl = getRealPathFromURI1(imageUri);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 20, bytes);
                byte[] byteArray = bytes.toByteArray();
                uploadImageBitmap5 = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                bitmapHashMap5.put(5, uploadImageBitmap5);

                request(bitmapHashMap5);
                imageIV5.setImageBitmap(thumbnail);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else if (requestCode == PICK_IMAGE_REQUEST5 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                //Getting the Bitmap from Gallery
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                bitmap.recycle();
//                profileImageBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                uploadImageBitmap6 = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                bitmapHashMap6.put(6, uploadImageBitmap6);

                //Setting the Bitmap to ImageView
                imageIV6.setImageBitmap(uploadImageBitmap6);
                request(bitmapHashMap6);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 66 && resultCode == RESULT_OK) {
            // Uri filePath = data.getData();
            Bitmap thumbnail;
            try {
//                ShrinkBitmap(data.toString(),300,300);
                thumbnail = MediaStore.Images.Media.getBitmap(
                        getContentResolver(), imageUri);
                imageurl = getRealPathFromURI1(imageUri);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 20, bytes);
                byte[] byteArray = bytes.toByteArray();
                uploadImageBitmap6 = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                bitmapHashMap6.put(6, uploadImageBitmap6);

                request(bitmapHashMap6);
                imageIV6.setImageBitmap(thumbnail);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        else if (requestCode == PICK_IMAGE_REQUEST6 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                //Getting the Bitmap from Gallery
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                bitmap.recycle();
//                profileImageBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                uploadImageBitmap7 = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                bitmapHashMap7.put(7, uploadImageBitmap7);

                //Setting the Bitmap to ImageView
                imageIV7.setImageBitmap(uploadImageBitmap7);
                request(bitmapHashMap7);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 77 && resultCode == RESULT_OK) {
            // Uri filePath = data.getData();
            Bitmap thumbnail;
            try {
//                ShrinkBitmap(data.toString(),300,300);
                thumbnail = MediaStore.Images.Media.getBitmap(
                        getContentResolver(), imageUri);
                imageurl = getRealPathFromURI1(imageUri);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 20, bytes);
                byte[] byteArray = bytes.toByteArray();
                uploadImageBitmap7 = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                bitmapHashMap7.put(7, uploadImageBitmap7);

                request(bitmapHashMap7);
                imageIV7.setImageBitmap(thumbnail);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        else if (requestCode == PICK_IMAGE_REQUEST7 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                //Getting the Bitmap from Gallery
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                bitmap.recycle();
//                profileImageBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                uploadImageBitmap8 = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                bitmapHashMap8.put(8, uploadImageBitmap8);

                //Setting the Bitmap to ImageView
                imageIV8.setImageBitmap(uploadImageBitmap8);
                request(bitmapHashMap8);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 88 && resultCode == RESULT_OK) {
            // Uri filePath = data.getData();
            Bitmap thumbnail;
            try {
//                ShrinkBitmap(data.toString(),300,300);
                thumbnail = MediaStore.Images.Media.getBitmap(
                        getContentResolver(), imageUri);
                imageurl = getRealPathFromURI1(imageUri);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 20, bytes);
                byte[] byteArray = bytes.toByteArray();
                uploadImageBitmap8 = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                bitmapHashMap8.put(8, uploadImageBitmap8);

                request(bitmapHashMap8);
                imageIV8.setImageBitmap(thumbnail);
            } catch (Exception e) {
                e.printStackTrace();
            }
//            try {
//
//                uploadImageBitmap8 = BitmapFactory.decodeFile(captured_image);
//                bitmapHashMap8.put(8, uploadImageBitmap8);
//                imageIV8.setImageBitmap(uploadImageBitmap8);
//                request(bitmapHashMap8);
//                Log.e("hashmap Size", bitmapHashMap8.size() + "");
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void request(HashMap<Integer, Bitmap> bitmapHashMap) {

        MyProgressDialog.showDialog(this);
        MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
        File file = null;
        if (bitmapHashMap == null) {
            Toast.makeText(PhotosChooserActivity.this, "select image", Toast.LENGTH_SHORT).show();


        } else {

            for (Bitmap bitmap : bitmapHashMap.values()) {
                bitmap.getAllocationByteCount();
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                Log.e("rsponse..............",bitmap.toString());
                file = Filetobitmap.bitmapToFile(PhotosChooserActivity.this, "file" + i+".jpeg", bitmap, true);

                if (file != null) {
                    entityBuilder.addBinaryBody(file.getName()+i, file, ContentType.create(file.getName()+"/jpeg") , file.getName() + ".jpeg");
                    i++;
                    Log.e("rsponse.....", file.toString());
//                entityBuilder.addBinaryBody("extension",file,ContentType.create("image/jpeg"),"jpeg");
                }
            }
        }
//        entityBuilder.addTextBody("user_id", user_id);
        //entityBuilder.addTextBody("uploadFile",file.toString());
        entityBuilder.addPart("uploadFile",new FileBody(new File(file.getPath())));
//        entityBuilder.addBinaryBody("file" + i, file, ContentType.MULTIPART_FORM_DATA , file.getName() + ".jpeg");
//        entityBuilder.addBinaryBody("uploadFile",file);
//        Log.e("rsponse", captured_image);
        entityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

        entityBuilder.setLaxMode().setBoundary("xx").setCharset(Charset.forName("UTF-8"));
        Log.e("rsponse", entityBuilder.toString());


        final PhotoMultipartRequest request = new PhotoMultipartRequest(NetworkConstant.upload_image, entityBuilder, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MyProgressDialog.hideDialog();

                Log.e("rsponse", "onResponse " + response);

                JSONArray jsonArray = null;
                try {
                    jsonArray = response.getJSONArray("file");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        String file_desti = jsonObject1.getString("fd");
                        picfds.add(file_desti);

                        Log.d("file_desti", picfds.toString());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    MyProgressDialog.hideDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (error.getMessage() != null)
                        Toast.makeText(PhotosChooserActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

                }
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(10000000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        imagequeue.add(request);

    }
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }
    public Uri setImageUri() {
        // Store image in dcim
        File file = new File(Environment.getExternalStorageDirectory() + "/DCIM/", "image" + new Date().getTime() + ".png");
        Uri imgUri = Uri.fromFile(file);
        this.captured_image = file.getAbsolutePath();
        return imgUri;
    }
    private void uploadImageArray(){
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, NetworkConstant.upload_all_pics, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.e("response", s);
                Toast.makeText(PhotosChooserActivity.this, "Images Upload Successfully", Toast.LENGTH_SHORT).show();
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }) {
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
              int  mStatusCode = response.statusCode;
                return super.parseNetworkResponse(response);
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", sharedPreferences.getString("update_id", ""));
                params.put("photos", picfds.toString());
                Log.d("params",params.toString());
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }



        imageIV1.setImageBitmap(thumbnail);
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    Bitmap ShrinkBitmap(String file, int width, int height){

        BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
        bmpFactoryOptions.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);

        int heightRatio = (int)Math.ceil(bmpFactoryOptions.outHeight/(float)height);
        int widthRatio = (int)Math.ceil(bmpFactoryOptions.outWidth/(float)width);

        if (heightRatio > 1 || widthRatio > 1)
        {
            if (heightRatio > widthRatio)
            {
                bmpFactoryOptions.inSampleSize = heightRatio;
            } else {
                bmpFactoryOptions.inSampleSize = widthRatio;
            }
        }
        bmpFactoryOptions.inJustDecodeBounds = false;
        bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
        Log.d("bitmap",bmpFactoryOptions.toString());
        Log.d("file",file.toString());
        bitmapHashMap1.put(1, bitmap);
        imageIV1.setImageBitmap(bitmap);
        request(bitmapHashMap1);
        return bitmap;
    }


}




