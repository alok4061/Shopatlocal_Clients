package com.example.hp.myapp.Utilities;



public class NetworkConstant {

  static String BASE_URL = "http://192.168.1.13:1337/";
  static String IMAGE_BASE_URL="http://192.168.1.13:1339/";

    public static final String csrf_token = BASE_URL + "csrfToken";//get
    public static final String signup = BASE_URL + "api/client/signup";//post
    public static final String login = BASE_URL + "api/client/login";//put
    public static final String personal_details=BASE_URL+"api/client/detail";//post//put//get
    public static final String category=BASE_URL+"api/option/categories";//get
    public static final String locationapi = BASE_URL + "api/client/detail/location";//Post
    public static final String upload_image = IMAGE_BASE_URL + "file/upload";//Post
    public static final String post_offers =BASE_URL +"api/client/detail/offer";//post
    public static final String delete_offers =BASE_URL +"api/client/detail/offer";//DELETE
    public static final String offers_list = BASE_URL + "api/client/detail/uniOffer";//Get
    public static final String id_proof_save = BASE_URL + "api/client/detail/proof";//Post
    public static final String id_proof_update = BASE_URL + "api/client/detail/proof";//Put
    public static final String check_id = BASE_URL + "api/client/getId";//Put
    public static final String upload_all_pics =BASE_URL+"api/client/detail/photos";//PUT
    public static final String save_addtional_info=BASE_URL+"api/client/detail/info";//post//put


}
