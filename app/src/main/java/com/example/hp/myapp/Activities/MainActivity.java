package com.example.hp.myapp.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.hp.myapp.R;
import com.example.hp.myapp.Utilities.Constants;
import com.example.hp.myapp.Utilities.MyProgressDialog;
import com.example.hp.myapp.Utilities.NetworkConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.hp.myapp.Utilities.ApiClient.BASE_URL;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    EditText mobiletxt;
    EditText pssval;
    RequestQueue loginQueue;
    RequestQueue requestQueue;
    String token34, mobile, passs;
    int mStatusCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        requestStoragePermission();
        requestQueue = Volley.newRequestQueue(this);
        loginQueue = Volley.newRequestQueue(this);
        init();

    }

    private void init() {
        TextView signup = (TextView) findViewById(R.id.signupbtn);
        mobiletxt = findViewById(R.id.mobileedittxtvalue);
        pssval = findViewById(R.id.passtxtvalue);
        Button login = (Button) findViewById(R.id.loginbtn);
        signup.setOnClickListener(this);
        login.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signupbtn:
                Intent signintent = new Intent(this, RegistrationForm.class);
                startActivity(signintent);
                break;
            case R.id.loginbtn:
                mobile = mobiletxt.getText().toString();
                passs = pssval.getText().toString();
                if (mobile.equals("")) {
                    Toast.makeText(this, "Please enter the mobile number", Toast.LENGTH_SHORT).show();
                } else if (passs.equals("")) {
                    Toast.makeText(this, "Please enter the password", Toast.LENGTH_SHORT).show();
                } else {
                    loginapi();
                }
                break;

        }
    }

    private void loginapi() {
        Log.d("api login", "Api call start ");
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, NetworkConstant.login, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.d("res123", s);
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(s);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    if (mStatusCode == 200) {
                        String iddata = jsonObject.getString("id");
                        String pass = jsonObject.getString("encryptedPassword");
                        SharedPreferences sharedPreferences = getSharedPreferences(Constants.USER_DETAIL, MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("User_id", iddata);
                        editor.apply();
                        Intent intent = new Intent(MainActivity.this, DashboardActivity.class);
                        startActivity(intent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                String responseBody = null;
                try {
                responseBody = new String(volleyError.networkResponse.data, "utf-8");
                JSONObject jsonObject = new JSONObject(responseBody);
                if (jsonObject.getString("status").equals("Unsuccess")) {
                    Toast.makeText(MainActivity.this, "Mobile number and Password are not match", Toast.LENGTH_SHORT).show();
                }
                Log.e("res123.......", jsonObject.toString());
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        }) {
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                mStatusCode = response.statusCode;
                Log.d("res123.......", mStatusCode + "");
                return super.parseNetworkResponse(response);
            }


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("phone", mobile);
                params.put("password", passs);
                Log.d("params", params.toString());
                return params;
            }
        };
        loginQueue.add(stringRequest);
    }
//    private void getToken(){
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, NetworkConstant.csrf_token, null, new com.android.volley.Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject jsonObject) {
//                try {
//                    token34 =jsonObject.getString("_csrf");
//                    Log.d("token",token34);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new com.android.volley.Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError volleyError) {
//
//            }
//        });
//        requestQueue.add(jsonObjectRequest);
//    }

//    private void sendRequest() {
//        RequestQueue queue = Volley.newRequestQueue(this);
//
//        final JSONObject jsonObject = new JSONObject();
//        try {
//            jsonObject.put("phone", "123456");
//            jsonObject.put("password", "123456");
//        } catch (JSONException e) {
//            // handle exception
//        }
//
//
//        JsonObjectRequest putRequest = new JsonObjectRequest(Request.Method.PUT, NetworkConstant.login, jsonObject,
//                new Response.Listener<JSONObject>()
//                {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        // response
//                        Log.d("Response", response.toString());
//                    }
//                },
//                new Response.ErrorListener()
//                {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        // error
//                        Log.d("Error.Response", error.toString());
//                    }
//                }
//        ) {
//
//            @Override
//            public Map<String, String> getHeaders()
//            {
//                Map<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json; charset=utf-8");
//                //headers.put("Content-Type", "application/json");
//                //headers.put("Accept", "application/json");
//                headers.put("x-csrf-token",token34);
//                return headers;
//            }
//        };
//
//        queue.add(putRequest);
//    }
//


    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            return;

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 12);
    }


    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == 12) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
//                Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
    }

}