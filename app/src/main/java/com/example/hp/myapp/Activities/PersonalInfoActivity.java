package com.example.hp.myapp.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.hp.myapp.R;
import com.example.hp.myapp.Utilities.Constants;
import com.example.hp.myapp.Utilities.NetworkConstant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PersonalInfoActivity extends AppCompatActivity implements View.OnClickListener,AdapterView.OnItemSelectedListener {
    EditText refertxt, nameofofctxt, nameofownertxt, contactperson;
    Spinner owenwespin, typeofmarrospin, catogoryspinn, packspin, tenurespin, subcatspin, modeofpayspin, venuespin;
    Button savebtn, updatebtn;
    RequestQueue saveQueue, updatequeue, categoryQueue;
    int mStatusCode;
    SharedPreferences sharedPreferences, updatesharepre,locpreference;
    ArrayList ownerlist = new ArrayList();
    ArrayList typeofmarketlist = new ArrayList();
    ArrayList typeofvenlist = new ArrayList();
    ArrayList categolist = new ArrayList();
    ArrayList subcatlist = new ArrayList();
    ArrayList tenuelist = new ArrayList();
    ArrayList packlist = new ArrayList();
    ArrayList modeofpaylist = new ArrayList();
    int catposi = 0;
    ArrayList subcategorylist = new ArrayList();
    double latitude =0.0;
    double lngitude =0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_info);
        saveQueue = Volley.newRequestQueue(this);
        updatequeue = Volley.newRequestQueue(this);
        sharedPreferences = getSharedPreferences(Constants.USER_DETAIL, MODE_PRIVATE);
        updatesharepre = getSharedPreferences(Constants.USER_DETAIL, MODE_PRIVATE);
        locpreference= getSharedPreferences(Constants.LOC_DATA,MODE_PRIVATE);
        Log.e("mylat",locpreference.getString("lat",""));
        categoryQueue = Volley.newRequestQueue(this);
        settooldata();
        init();
        if (updatesharepre.contains("update_id")) {
            Log.d("asdfgh",updatesharepre.getString("update_id",""));
            savebtn.setVisibility(View.INVISIBLE);
        }
        additems();
        getCategory();
    }
    private void additems() {
        //ownerlist_item
        ownerlist.add("Select Ownership of Property");
        ownerlist.add("Rent/Lease");
        ownerlist.add("Own");
        //TypeofMarket
        typeofmarketlist.add("Select type of Market");
        typeofmarketlist.add("Shopping Mall");
        typeofmarketlist.add("Local Market");
        //TypeofVenue
        typeofvenlist.add("Select Type of Venue");
        typeofvenlist.add("Shop");
        typeofvenlist.add("Hawker");
        typeofvenlist.add("ShowRoom");
        typeofvenlist.add("Open Space");
        typeofvenlist.add("factory");
        typeofvenlist.add("Office");
        typeofvenlist.add("Food Corner");
        typeofvenlist.add("Other");
        //Mode of Payment
        modeofpaylist.add("Select Mode Of Pay");
        modeofpaylist.add("Cheque");
        modeofpaylist.add("Bank Transfer");
        modeofpaylist.add("mobile wallet");
        modeofpaylist.add("cash");
        //Package
        packlist.add("Select Package List");
        packlist.add("Copper");
        packlist.add("Silver");
        packlist.add("Gold");
        packlist.add("Diamond");
        packlist.add("Platinum");
        //Tenure
        tenuelist.add("Select Tenure");
        tenuelist.add("1 Year");
        tenuelist.add("2 Year");
        tenuelist.add("4 year");
        //Category
        categolist.add("Select Category");
        //Sub-category
        subcategorylist.add("Select Sub-Category");
        ArrayAdapter<String> owneradaptor = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, ownerlist);
        ArrayAdapter<String> tymarketadaptor = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, typeofmarketlist);
        ArrayAdapter<String> tyofvenueadaptor = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, typeofvenlist);
        ArrayAdapter<String> modeofpayadaptor = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, modeofpaylist);
        ArrayAdapter<String> packageadaptor = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, packlist);
        ArrayAdapter<String> tenueadaptor = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, tenuelist);
        ArrayAdapter<String> categoryadaptor = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, categolist);
        ArrayAdapter<String> subcatadaptor = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, subcategorylist);
        owenwespin.setAdapter(owneradaptor);
        typeofmarrospin.setAdapter(tymarketadaptor);
        venuespin.setAdapter(tyofvenueadaptor);
        modeofpayspin.setAdapter(modeofpayadaptor);
        packspin.setAdapter(packageadaptor);
        tenurespin.setAdapter(tenueadaptor);
        catogoryspinn.setAdapter(categoryadaptor);
        subcatspin.setAdapter(subcatadaptor);

    }
    private void init() {
        refertxt = findViewById(R.id.refer_txt);
        nameofofctxt = findViewById(R.id.nameofoffice_txt);
        nameofownertxt = findViewById(R.id.owener_txt);
        contactperson = findViewById(R.id.contact_person_txt);
        owenwespin = findViewById(R.id.owenershipspin);
        typeofmarrospin = findViewById(R.id.marketspin);
        catogoryspinn = findViewById(R.id.categoryspin);
        tenurespin = findViewById(R.id.tenurespin);
        packspin = findViewById(R.id.packagespin);
        savebtn = findViewById(R.id.loca_submit_btn);
        subcatspin = findViewById(R.id.subcategoryspin);
        modeofpayspin = findViewById(R.id.modeofpayspin);
        venuespin = findViewById(R.id.vanuesspin);
        updatebtn = findViewById(R.id.update_submit_btn);
        savebtn.setOnClickListener(this);
        updatebtn.setOnClickListener(this);
        catogoryspinn.setOnItemSelectedListener(this);
        subcatspin.setOnItemSelectedListener(this);
    }

    private void settooldata() {
        ImageView imageView = findViewById(R.id.toolimage);
        TextView tooltxt = findViewById(R.id.tooltxt);
        tooltxt.setText("Personal Information");
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.loca_submit_btn:
                savedata_api();
                break;
            case R.id.update_submit_btn:
                updatedata_api();
                break;
        }
    }

    private void savedata_api() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, NetworkConstant.personal_details, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.d("res123", s);
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(s);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    if (mStatusCode == 200) {
                        String iddata = jsonObject.getString("id");
                        SharedPreferences.Editor editor = updatesharepre.edit();
                        editor.putString("update_id", iddata);
                        editor.apply();
                        Toast.makeText(PersonalInfoActivity.this,"Details Successfully Saved",Toast.LENGTH_SHORT).show();
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                String responseBody = null;
                try {
                    responseBody = new String(volleyError.networkResponse.data, "utf-8");
                    JSONObject jsonObject = new JSONObject(responseBody);
                    if (jsonObject.getString("status").equals("Unsuccess")) {
                        Toast.makeText(PersonalInfoActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
                    }
                    Log.e("res123.......", jsonObject.toString());
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }) {
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                mStatusCode = response.statusCode;
                Log.d("res123.......", mStatusCode + "");
                return super.parseNetworkResponse(response);
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("clientId", sharedPreferences.getString("User_id", ""));
                params.put("reference", refertxt.getText().toString());
                params.put("nameOfOffice", nameofofctxt.getText().toString());
                params.put("typeOfMarket", typeofmarrospin.getSelectedItem().toString());
                params.put("typeOfVenue", venuespin.getSelectedItem().toString());
                params.put("category", catogoryspinn.getSelectedItem().toString());
                params.put("subCategory", subcatspin.getSelectedItem().toString());
                params.put("tenure", tenurespin.getSelectedItem().toString());
                params.put("package", packspin.getSelectedItem().toString());
                params.put("modeOfpayment", modeofpayspin.getSelectedItem().toString());
                params.put("contactPerson", contactperson.getText().toString());
                params.put("ownerShipOfProperty", owenwespin.getSelectedItem().toString());
                params.put("nameOfOwner", nameofownertxt.getText().toString());
//                params.put("latitude",locpreference.getString("lat",""));
//                params.put("longitude", locpreference.getString("lng",""));
                params.put("latitude","28.6618976");
                params.put("longitude", "77.22739580000007");
                Log.d("params", params.toString());
                return params;
            }
        };
        saveQueue.add(stringRequest);
    }

    private void updatedata_api() {
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, NetworkConstant.personal_details, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.d("res123", s);
                Toast.makeText(PersonalInfoActivity.this,"Details Successfully Updated",Toast.LENGTH_SHORT).show();
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("res123.......", volleyError.toString());
            }
        }) {
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                mStatusCode = response.statusCode;
                Log.d("res123.......", mStatusCode + "");
                return super.parseNetworkResponse(response);
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("id", updatesharepre.getString("update_id", ""));
                params.put("reference", URLEncoder.encode(refertxt.getText().toString()));
                params.put("nameOfOffice", URLEncoder.encode(nameofofctxt.getText().toString()));
                params.put("typeOfMarket", URLEncoder.encode(typeofmarrospin.getSelectedItem().toString()));
                params.put("typeOfVenue", URLEncoder.encode(nameofofctxt.getText().toString()));
                params.put("category", URLEncoder.encode(catogoryspinn.getSelectedItem().toString()));
                params.put("subCategory", URLEncoder.encode(subcatspin.getSelectedItem().toString()));
                params.put("tenure", URLEncoder.encode(tenurespin.getSelectedItem().toString()));
                params.put("package", URLEncoder.encode(packspin.getSelectedItem().toString()));
                params.put("modeOfpayment", URLEncoder.encode(modeofpayspin.getSelectedItem().toString()));
                params.put("contactPerson", URLEncoder.encode(contactperson.getText().toString()));
                params.put("ownerShipOfProperty", URLEncoder.encode(owenwespin.getSelectedItem().toString()));
                params.put("nameOfOwner", URLEncoder.encode(nameofownertxt.getText().toString()));
                params.put("latitude","28.6618976");
                params.put("longitude", "77.22739580000007");
                Log.d("datasddf",updatesharepre.getString("update_id", ""));
                Log.d("params", params.toString());
                return params;
            }
        };
        saveQueue.add(stringRequest);
    }

    private void getCategory() {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(NetworkConstant.category, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray jsonArray) {
                Log.d("category", jsonArray.toString());

                for (int i = 0; i < jsonArray.length(); i++) {
                    try {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String cat_name = jsonObject.getString("name");
                        categolist.add(cat_name);
                        JSONArray jsonArray1 = jsonObject.getJSONArray("subs");
                        subcatlist.add(jsonArray1);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
        categoryQueue.add(jsonArrayRequest);
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        switch (adapterView.getId()){
            case R.id.categoryspin:
                if (catogoryspinn.getSelectedItemPosition()==0){
                    subcategorylist.clear();
                    subcategorylist.add("Select Sub Category");
                }
                else {
                    subcategorylist.clear();
                    subcategorylist.add("Select Sub Category");
                    catposi = catogoryspinn.getSelectedItemPosition();

                    if (subcatlist.size() > 0) {
                        String sun_string =  subcatlist.get(catposi-1).toString();
                        Log.d("list1234",subcatlist.get(catposi-1).toString() );
                        JSONArray jsonArray = null;
                        try {
                            jsonArray = new JSONArray(sun_string);
                            for (int j = 0; j < jsonArray.length(); j++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(j);
                                String name = jsonObject.getString("name");
                                subcategorylist.add(name);
                                Log.d("name", name);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;
            case R.id.subcategoryspin:

//                if (subcatspin.getSelectedItemPosition()==0){
//                    subcategorylist.clear();
//                    subcategorylist.add("Select Sub Category");
//                }
//                else {
//                    subcategorylist.clear();
//                    subcategorylist.add("Select Sub Category");
//                    catposi = catogoryspinn.getSelectedItemPosition();
//                    Toast.makeText(this,catposi+"",Toast.LENGTH_SHORT).show();
//                    if (subcatlist.size() > 0) {
//                        String sun_string =  subcatlist.get(catposi-1).toString();
//                        JSONArray jsonArray = null;
//                        try {
//                            jsonArray = new JSONArray(sun_string);
//                            for (int j = 0; j < jsonArray.length(); j++) {
//                                JSONObject jsonObject = jsonArray.getJSONObject(j);
//                                String name = jsonObject.getString("name");
//                                subcategorylist.add(name);
//                                Log.d("name", name);
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }

                break;
        }
    }
    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
