package com.example.hp.myapp.Activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.example.hp.myapp.Adaptors.OfferListAdaptor;
import com.example.hp.myapp.Controller.AppController;
import com.example.hp.myapp.R;
import com.example.hp.myapp.Utilities.Constants;
import com.example.hp.myapp.Utilities.MyProgressDialog;
import com.example.hp.myapp.Utilities.NetworkConstant;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class OffersActivity extends AppCompatActivity implements View.OnClickListener {
    ArrayList iddatalist = new ArrayList();
    ArrayList tittlelist = new ArrayList();
    ArrayList actilist = new ArrayList();
    ArrayList disclist = new ArrayList();
    ArrayList validlist = new ArrayList();
    ArrayList discriptionlist = new ArrayList();
    Button save;
    AlertDialog alertDialog;
    int mStatusCode;
    SharedPreferences sharedPreferences;
    String titalvalue, actualvalue, disvalue, validvalue,descripvalue;
    RequestQueue postqueue, fatchlist, deletelist;
    SwipeMenuListView offerlist;
    OfferListAdaptor offerListAdaptor;
    SharedPreferences clientDetailid;
    Calendar myCalendar;
    TextView valid;
    DatePickerDialog.OnDateSetListener date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offers);
        settooldata();
//        ListView offerlist = findViewById(R.id.offerslist);
        sharedPreferences = getSharedPreferences(Constants.USER_DETAIL, MODE_PRIVATE);
        postqueue = Volley.newRequestQueue(this);
        fatchlist = Volley.newRequestQueue(this);
        deletelist = Volley.newRequestQueue(this);
        Log.d("Log", sharedPreferences.getString("update_id", ""));
        offerlist = (SwipeMenuListView) findViewById(R.id.offerslist);
//        fatchlistoffers_api();
        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        Button add = findViewById(R.id.offferaddbtn);
        add.setOnClickListener(this);

        SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.RED));
                // set item width
                deleteItem.setWidth(170);

                // set a icon
                deleteItem.setIcon(R.drawable.ic_phone);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };
        offerlist.setMenuCreator(creator);
        offerlist.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                if (iddatalist.size()==0){
                   fatchlistoffers_api();

               }
               else {
                   String getid = iddatalist.get(position).toString();
                   Log.d("myid.....",getid);
                   deleteoffers_api(getid);
               }
                return false;
            }
        });
    }
    private void settooldata(){
        ImageView imageView = findViewById(R.id.toolimage);
        TextView tooltxt = findViewById(R.id.tooltxt);
        tooltxt.setText("Offers");
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.offferaddbtn:
                myCalendar = Calendar.getInstance();
                final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
                View view1 = LayoutInflater.from(view.getContext()).inflate(R.layout.offeraddition, null);
                final EditText titletxt = view1.findViewById(R.id.offertitle);
                final EditText actual = view1.findViewById(R.id.acu_edit_txt);
                final EditText discount = view1.findViewById(R.id.dis_edit_txt);
                valid = view1.findViewById(R.id.valid_edit_txt);
                final EditText discrip = view1.findViewById(R.id.discri_edit_txt);
                Button savebtn = view1.findViewById(R.id.saveoffer_btn);
                valid.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                     DatePickerDialog datePickerDialog=   new DatePickerDialog(OffersActivity.this, date, myCalendar
                                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                                myCalendar.get(Calendar.DAY_OF_MONTH));
                     datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                     datePickerDialog.show();

                    }
                });

                savebtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        titalvalue = titletxt.getText().toString();
                        actualvalue = actual.getText().toString();
                        disvalue = discount.getText().toString();
                        validvalue = valid.getText().toString();
                        descripvalue =discrip.getText().toString();
                        Log.d("params", titalvalue + actualvalue + disvalue + validvalue);
                        addoffers_api(titalvalue, actualvalue, disvalue, validvalue);
                        alertDialog.dismiss();
                    }
                });
                dialogBuilder.setView(view1);
                alertDialog = dialogBuilder.create();
                alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                alertDialog.show();
                break;

        }
    }

    private void addoffers_api(final String tital, final String actprz, final String disprz, final String validdate) {
        MyProgressDialog.showDialog(this);
        Log.d("params", tital + actprz + disprz + validdate);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, NetworkConstant.post_offers, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                MyProgressDialog.hideDialog();
                Log.d("res123", s);
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(s);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    if (mStatusCode == 200) {
                        String iddata = jsonObject.getString("id");
//                      String pass =jsonObject.getString("encryptedPassword");
                        clientDetailid = getSharedPreferences(Constants.USER_DETAIL, MODE_PRIVATE);
                        SharedPreferences.Editor editor = clientDetailid.edit();
                        editor.putString("client_detail", iddata);
                        editor.apply();
                        fatchlistoffers_api();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                MyProgressDialog.hideDialog();
                String responseBody = null;
                if (responseBody.equals(null)){
                    Toast.makeText(OffersActivity.this,"Something went Wrong",Toast.LENGTH_SHORT).show();
                }
                else {
                    try {
                        responseBody = new String(volleyError.networkResponse.data, "utf-8");
                        JSONObject jsonObject = new JSONObject(responseBody);
                        if (jsonObject.getString("status").equals("Unsuccess")) {
                            Toast.makeText(OffersActivity.this, "Please Try Again\n ", Toast.LENGTH_SHORT).show();
                        }
                        Log.e("res123.......", jsonObject.toString());
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }) {
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                mStatusCode = response.statusCode;
                Log.d("res123.......", mStatusCode + "");
                return super.parseNetworkResponse(response);
            }
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("Log", sharedPreferences.getString("update_id", ""));
                params.put("clientDetailId", sharedPreferences.getString("update_id", ""));
                params.put("title", tital);
                params.put("actualPrice", actprz);
                params.put("discountPrice", disprz);
                params.put("validTill", validdate);
                params.put("description", descripvalue);
                Log.d("params", params.toString());
                return params;
            }
        };
        postqueue.add(stringRequest);
    }
    private void fatchlistoffers_api() {
        iddatalist.clear();
        tittlelist.clear();
        actilist.clear();
        disclist.clear();
        validlist.clear();
        discriptionlist.clear();
        MyProgressDialog.showDialog(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(NetworkConstant.offers_list + "?clientDetailId=" + sharedPreferences.getString("update_id", ""), new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray jsonArray) {
                MyProgressDialog.hideDialog();
                for (int i = 0; i < jsonArray.length(); i++) {
                    try {
                        if(iddatalist.size()<0){
                            finish();
                        }
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String title = jsonObject.getString("title");
                            String actual = jsonObject.getString("actualPrice");
                            String discountprz = jsonObject.getString("discountPrice");
                            String validtill = jsonObject.getString("validTill");
                            String descrival =jsonObject.getString("description");
                            iddatalist.add(id);
                            tittlelist.add(title);
                            actilist.add(actual);
                            disclist.add(discountprz);
                            validlist.add(validtill);
                            discriptionlist.add(descrival);
                            offerListAdaptor = new OfferListAdaptor(OffersActivity.this, tittlelist, actilist, disclist, validlist, discriptionlist);
                            offerlist.setAdapter(offerListAdaptor);
                            Log.d("listvalue", id + title + actual + discountprz + validtill);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                MyProgressDialog.hideDialog();

            }
        });
        fatchlist.add(jsonArrayRequest);
    }

    @Override
    protected void onStart() {
        super.onStart();
        fatchlistoffers_api();
    }

    private void deleteoffers_api(final String ide) {

        MyProgressDialog.showDialog(this);
        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, NetworkConstant.delete_offers+"?id="+ide, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                MyProgressDialog.hideDialog();

                fatchlistoffers_api();


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                MyProgressDialog.hideDialog();

            }
        })
        ;
        deletelist.add(stringRequest);
    }
    private void updateLabel() {
        String myFormat = "MM/dd/yy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        valid.setText(sdf.format(myCalendar.getTime()));
    }
}



