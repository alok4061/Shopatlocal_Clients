package com.example.hp.myapp.Activities;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.hp.myapp.R;
import com.example.hp.myapp.Utilities.Constants;
import com.example.hp.myapp.Utilities.NetworkConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class AdditionalInfoActivity extends AppCompatActivity implements View.OnClickListener{
    EditText distxt, hoursofoperctxt, modeofpayrtxt, addito_info;
    Button savebtn,updatebtn;
    int mStatusCode;
    SharedPreferences sharedPreferences,sharedPreferences1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_additional_info);
        sharedPreferences = getSharedPreferences(Constants.USER_DETAIL,MODE_PRIVATE);
        sharedPreferences1 =getSharedPreferences(Constants.USER_DETAIL,MODE_PRIVATE);

        if (sharedPreferences1.contains("id_add_id")){
            savebtn.setVisibility(View.INVISIBLE);
        }
        init();
    }
    private void init(){
        distxt = findViewById(R.id.refer_txt);
        hoursofoperctxt = findViewById(R.id.nameofoffice_txt);
        modeofpayrtxt = findViewById(R.id.owener_txt);
        addito_info = findViewById(R.id.contact_person_txt);
        savebtn = findViewById(R.id.loca_submit_btn);
        updatebtn = findViewById(R.id.update_submit_btn);
        savebtn.setOnClickListener(this);
        updatebtn.setOnClickListener(this);
    }
    private void savedata_api() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, NetworkConstant.save_addtional_info, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.d("res123", s);
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(s);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    if (mStatusCode == 200) {
                        String iddata = jsonObject.getString("id");

                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("id_add_id", iddata);
                        editor.apply();
                        Toast.makeText(AdditionalInfoActivity.this,"Details Successfully Saved",Toast.LENGTH_SHORT).show();
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                String responseBody = null;
                try {
                    responseBody = new String(volleyError.networkResponse.data, "utf-8");
                    JSONObject jsonObject = new JSONObject(responseBody);
                    if (jsonObject.getString("status").equals("Unsuccess")) {
                        Toast.makeText(AdditionalInfoActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
                    }
                    Log.e("res123.......", jsonObject.toString());
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }) {
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                mStatusCode = response.statusCode;
                Log.d("res123.......", mStatusCode + "");
                return super.parseNetworkResponse(response);
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("clientDetailId", sharedPreferences.getString("update_id", ""));
                params.put("description", distxt.getText().toString());
                params.put("hourOfOperation", hoursofoperctxt.getText().toString());
                params.put("modeOfPayment", modeofpayrtxt.getText().toString());
                params.put("additionalInfo", addito_info.getText().toString());
                Log.d("params", params.toString());
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void updatedata_api() {
        final StringRequest stringRequest = new StringRequest(Request.Method.PUT, NetworkConstant.save_addtional_info, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.d("res123", s);
                Toast.makeText(AdditionalInfoActivity.this,"Details Successfully Updated",Toast.LENGTH_SHORT).show();
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("res123.......", volleyError.toString());
            }
        }) {
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                mStatusCode = response.statusCode;
                Log.d("res123.......", mStatusCode + "");
                return super.parseNetworkResponse(response);
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", sharedPreferences.getString("id_add_id", ""));
                params.put("description", distxt.getText().toString());
                params.put("hourOfOperation", hoursofoperctxt.getText().toString());
                params.put("modeOfPayment", modeofpayrtxt.getText().toString());
                params.put("additionalInfo", addito_info.getText().toString());
                Log.d("params", params.toString());
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.loca_submit_btn:
                savedata_api();
                break;
            case R.id.update_submit_btn:
                updatedata_api();
                break;
        }

    }
}
