package com.example.hp.myapp.Modals;

import android.content.Context;
import android.graphics.Bitmap;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by hp on 8/2/2017.
 */

public class Filetobitmap {
    public static File bitmapToFile(Context context, String filename, Bitmap bitmap, boolean compress) {
        File f = new File(context.getCacheDir(), filename);
        try {
            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        if (compress) {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 45, bos);
        } else {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
        }
        byte[] bitmapdata = bos.toByteArray();

        //write the bytes in file
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return f;

    }

}

