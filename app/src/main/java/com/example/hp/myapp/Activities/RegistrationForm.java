package com.example.hp.myapp.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.hp.myapp.R;
import com.example.hp.myapp.Utilities.Constants;
import com.example.hp.myapp.Utilities.MyProgressDialog;
import com.example.hp.myapp.Utilities.NetworkConstant;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

public class RegistrationForm extends AppCompatActivity implements View.OnClickListener {
    EditText name,emailid,mobile,password;
    RequestQueue requestQueue;
    private String token12;
    private int mStatusCode;
    TextView sign_logbtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_form);
        requestQueue = Volley.newRequestQueue(this);
        Intent intent =getIntent();
//         token12 = intent.getStringExtra("tokens");
//         Log.d("tokenvalue",token12);
        Button signlog = findViewById(R.id.signin_btn);
         name = findViewById(R.id.regusername);
        emailid=  findViewById(R.id.regemailid);
         mobile = findViewById(R.id.regnumber);
        password = findViewById(R.id.regpassword);
        sign_logbtn =findViewById(R.id.sign_log);
        signlog.setOnClickListener(this);
        sign_logbtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.signin_btn:
                String mobiletxt = mobile.getText().toString();
                String usertxt = name.getText().toString();
                final String emailtxt =emailid.getText().toString();
                String passwordtxt = password.getText().toString();
                if (mobiletxt.equals("")||usertxt.equals("")||emailtxt.equals("")||passwordtxt.equals("")){
                    Toast.makeText(this,"Please fill the fields",Toast.LENGTH_SHORT).show();
                }else {
                    signupapi(mobiletxt,emailtxt,usertxt,passwordtxt);
                }
                break;
            case R.id.sign_log:
                Intent intent = new Intent(this,MainActivity.class);
                startActivity(intent);
                break;
        }
    }
    private void signupapi(String mob,String email,String username,String passw){
        JSONObject jsonObject = new JSONObject();
        try {
//            jsonObject.put("_csrf", token12);
            jsonObject.put("name",username );
            jsonObject.put("email",email);
            jsonObject.put("phone", mob);
            jsonObject.put("password", passw);

        }
        catch (JSONException e) {
            e.printStackTrace();
        }


        MyProgressDialog.showDialog(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, NetworkConstant.signup,jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                MyProgressDialog.hideDialog();
                try {
                    if (mStatusCode==200){
                        Log.d("response",jsonObject.toString());
                        String name =jsonObject.getString("name");
                        String emialid =jsonObject.getString("email");
                        String phone =jsonObject.getString("phone");
                        String pass =jsonObject.getString("encryptedPassword");
                        String id =jsonObject.getString("id");
                        SharedPreferences sharedPreferences = getSharedPreferences(Constants.USER_DETAIL,MODE_PRIVATE);
                        SharedPreferences.Editor editor =sharedPreferences.edit();
                        editor.putString("User_id",id);
                        editor.apply();
                        Intent intent = new Intent(RegistrationForm.this,DashboardActivity.class);
                        startActivity(intent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                MyProgressDialog.hideDialog();
                Log.d("error",volleyError.toString());

            }
        }){
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                mStatusCode = response.statusCode;
                Log.d("res123.......",mStatusCode+"");
                return super.parseNetworkResponse(response);
            }
        };
        requestQueue.add(jsonObjectRequest);

    }
}
