package com.example.hp.myapp.Adaptors;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.hp.myapp.R;

import java.util.ArrayList;

public class OfferListAdaptor extends ArrayAdapter{
    ArrayList tittle =new ArrayList();
    ArrayList acti =new ArrayList();
    ArrayList disc =new ArrayList();
    ArrayList valid =new ArrayList();
    ArrayList discription =new ArrayList();


    public OfferListAdaptor(@NonNull Context context, ArrayList tittle,ArrayList acti,ArrayList disc,ArrayList valid,ArrayList discription) {
        super(context, R.layout.activity_offer_list_adaptor,tittle);
        this.tittle =tittle;
        this.acti =acti;
        this.disc =disc;
        this.valid =valid;
        this.discription =discription;
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder = new ViewHolder();
        convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_offer_list_adaptor,parent,false);
        holder.titletxt = convertView.findViewById(R.id.offertitlevalue);
        holder.actiutxt = convertView.findViewById(R.id.actu_offer_rate);
        holder.descounttxtx = convertView.findViewById(R.id.dis_offer_rate);
        holder.valitxt = convertView.findViewById(R.id.offertillvalue);
        holder.distxt = convertView.findViewById(R.id.offerdescriptionvalue);
        holder.titletxt.setText(tittle.get(position).toString());
        holder.actiutxt.setText(acti.get(position).toString());
        holder.descounttxtx.setText(disc.get(position).toString());
        holder.valitxt.setText(valid.get(position).toString());
        holder.distxt.setText(discription.get(position).toString());
        convertView.setTag(holder);
        return convertView;
    }
    static class ViewHolder {
        TextView titletxt,actiutxt,descounttxtx,valitxt,distxt;
    }
}
