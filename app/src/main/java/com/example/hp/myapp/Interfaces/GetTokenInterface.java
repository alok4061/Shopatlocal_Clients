package com.example.hp.myapp.Interfaces;

import android.support.v7.util.SortedList;

import com.example.hp.myapp.Modals.TokenModal;

import java.util.List;

import retrofit2.Callback;
import retrofit2.http.GET;
import retrofit2.http.PUT;

/**
 * Created by hp on 7/31/2017.
 */

public interface GetTokenInterface {

    @GET("csrfToken")
     public void getTokens(Callback<List<TokenModal>> response);

}
