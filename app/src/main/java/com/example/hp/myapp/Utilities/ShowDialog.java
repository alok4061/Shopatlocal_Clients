package com.example.hp.myapp.Utilities;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

public class ShowDialog {

    private static ProgressDialog pDialog = null;

    public static void showDialog(Context context, String msg) {
        if (pDialog == null) {
            pDialog = new ProgressDialog(context);
            pDialog.setProgressDrawable(new ColorDrawable(Color.BLUE));
            pDialog.setMessage(msg);
            pDialog.setCancelable(false);
            if (!pDialog.isShowing()) {
                pDialog.show();
            }
        } else {
            pDialog.dismiss();
            pDialog = new ProgressDialog(context);
            pDialog.setProgressDrawable(new ColorDrawable(Color.BLUE));
            pDialog.setMessage(msg);
            pDialog.setCancelable(false);
            if (!pDialog.isShowing()) {
                pDialog.show();
            }
        }
    }

    public static void hideDialog() {
        if (pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }

}
