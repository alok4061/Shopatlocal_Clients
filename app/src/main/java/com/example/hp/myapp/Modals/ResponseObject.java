package com.example.hp.myapp.Modals;

import java.io.Serializable;

/**
 * Created by hp on 8/2/2017.
 */

public class ResponseObject implements Serializable
{

    private String pic_url;


    public String getPic_url() {
        return pic_url;
    }

    public void setPic_url(String pic_url) {
        this.pic_url = pic_url;
    }


}
