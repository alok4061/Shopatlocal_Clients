package com.example.hp.myapp.Utilities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;


/**
 * Created by hp on 8/16/2017.
 */

public class LacationUtils implements LocationListener {

    private LocationManager locationManager;
    private Location location ;
    private boolean netwrokEnabled, gpsEnabled;
    private double longitude = 0.0, latt = 0.0;
    LatLng latLng;

    public static void showToastLong(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static void showToastShort(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void showInfoLog(String message) {
        Log.i("arsh", message);
    }

    public static void showErrorLog(String message) {
        Log.e("arsh", message);
    }

    public static void showDebugLog(String message) {
        Log.d("arsh", message);
    }

    public static int dpToPx(int dp, DisplayMetrics displayMetrics) {
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    public static int pxToDp(int px, DisplayMetrics displayMetrics) {
        int dp = Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return dp;
    }

    public static boolean mobileNumber(String number) {
        if (number.length() < 10) {
            return false;
        }
        if (Patterns.PHONE.matcher(number).matches()) {
            return true;
        }
        return false;
    }

    public static boolean emailAddress(String number) {
        if (number.length() > 0) {
            if (Patterns.EMAIL_ADDRESS.matcher(number).matches()) {
                return true;
            }
        }
        return false;
    }

    public LatLng getlocation(final Context context) {
        latLng = new LatLng(latt, longitude);
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        gpsEnabled = locationManager.isProviderEnabled(locationManager.GPS_PROVIDER);
        netwrokEnabled = locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, false);
        ActivityCompat.checkSelfPermission(context, "android.permission.ACCESS_COARSE_LOCATION");
        ActivityCompat.checkSelfPermission(context, "android.permission.ACCESS_FINE_LOCATION");

        if (!gpsEnabled && !netwrokEnabled) {
            location = locationManager.getLastKnownLocation(provider);
            //no network provider
            AlertDialog.Builder builder = new AlertDialog.Builder(context).setMessage("Please turn on your GPS").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    context.startActivity(intent);
                }
            }).setCancelable(false);
            builder.show();
//            Toast.makeText(getApplicationContext(), "Please turn on the GPS for more accurate results", Toast.LENGTH_SHORT).show();
        } else {
            try {


                if (gpsEnabled) {
                    location = locationManager.getLastKnownLocation(provider);
                    if (location == null) {
                        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1, 10, this);
                        location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (locationManager != null) {
                            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                longitude = location.getLongitude();
                                latt = location.getLatitude();
                                latLng = new LatLng(latt, longitude);
                            }
                        }
                    }
                }
                if (netwrokEnabled) {
                    location = locationManager.getLastKnownLocation(provider);
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1, 10, this);
                    if (locationManager != null) {
                        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            longitude = location.getLongitude();
                            latt = location.getLatitude();
                            latLng = new LatLng(latt, longitude);
                        }
                    }
                }
                LacationUtils.showErrorLog(latt + " " + longitude);

            }
            catch (Exception e) {


            }

        }
        return latLng;
    }
            @Override
            public void onLocationChanged (Location location){
//        longitude = location.getLongitude();
//        latt = location.getLatitude();
//        latLng = new LatLng(latt, longitude);

            }

            @Override
            public void onStatusChanged (String s,int i, Bundle bundle){

            }

            @Override
            public void onProviderEnabled (String s){

            }

            @Override
            public void onProviderDisabled (String s){

            }


        }

